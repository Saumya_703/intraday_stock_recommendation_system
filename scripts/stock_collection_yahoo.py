import requests
import pandas as pd


class StockDataCollector:
    def __init__(self):
        self.base_url = "https://query1.finance.yahoo.com/v8/finance/chart"

    def collect_stock_data(self, symbol):
        # Make API request to fetch stock data
        url = f"{self.base_url}/{symbol}"
        params = {
            'range': '1d',  # Specify the time range for data (e.g., 1d, 1mo, 1y)
            'interval': '1d',  # Specify the interval for data (e.g., 1d, 1wk, 1mo)
        }
        response = requests.get(url, params=params)

        if response.status_code == 200:
            # Process the API response and extract relevant data
            data = response.json()
            if 'chart' in data:
                stock_data = data['chart']['result'][0]['indicators']['quote'][0]

                # Convert stock data to a DataFrame
                df = pd.DataFrame(stock_data)

                # Save the data as a CSV file
                file_path = f"data/raw/{symbol}.csv"
                df.to_csv(file_path, index=False)

                print(f"Stock data for {symbol} collected and saved.")
            else:
                print(f"No data found for {symbol}.")
        else:
            print(f"Failed to collect stock data for {symbol}. Error: {response.text}")


def main():
    # Create an instance of the StockDataCollector class
    collector = StockDataCollector()

    # List of stock symbols to collect data for
    stock_symbols = ['AAPL', 'GOOGL', 'MSFT']

    # Collect data for each stock symbol
    for symbol in stock_symbols:
        collector.collect_stock_data(symbol)


if __name__ == "__main__":
    main()
