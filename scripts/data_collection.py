import requests
import pandas as pd


class StockDataCollector:
    def __init__(self, api_url):
        self.api_url = api_url

    def collect_stock_data(self, symbol):
        # Make API request to fetch stock data
        url = f"{self.api_url}/stocks/{symbol}"
        response = requests.get(url)

        if response.status_code == 200:
            # Process the API response and extract relevant data
            data = response.json()
            stock_data = data['stock_data']

            # Convert stock data to a DataFrame
            df = pd.DataFrame(stock_data)

            # Save the data as a CSV file
            file_path = f"data/raw/{symbol}.csv"
            df.to_csv(file_path, index=False)

            print(f"Stock data for {symbol} collected and saved.")
        else:
            print(f"Failed to collect stock data for {symbol}. Error: {response.text}")


def main():
    # Create an instance of the StockDataCollector class
    collector = StockDataCollector(api_url="https://api.example.com")

    # List of stock symbols to collect data for
    stock_symbols = ['AAPL', 'GOOGL', 'MSFT']

    # Collect data for each stock symbol
    for symbol in stock_symbols:
        collector.collect_stock_data(symbol)


if __name__ == "__main__":
    main()
