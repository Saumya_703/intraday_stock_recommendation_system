# Stock Data Settings
STOCK_DATA_DIR = "data/raw/"  # Directory path for the raw stock data files
PROCESSED_DATA_DIR = "data/processed/"  # Directory path to store the processed stock data files

# Model Settings
MODEL_DIR = "data/models/"  # Directory path to store the trained models
MODEL_FILENAME = "svm_model.pkl"  # Filename for the trained SVM model

# Feature Engineering Settings
FEATURE_WINDOW = 30  # Number of days to consider for feature engineering

# Data Split Settings
TRAIN_SIZE = 0.8  # Percentage of data to use for training
VAL_SIZE = 0.1  # Percentage of data to use for validation
TEST_SIZE = 0.1  # Percentage of data to use for testing

# Preprocessing Settings
REMOVE_DUPLICATES = True  # Remove duplicate rows from the data
HANDLE_MISSING_DATA = True  # Handle missing data in the dataset
MISSING_DATA_STRATEGY = "mean"  # Strategy to handle missing data ("mean", "median", "drop")
