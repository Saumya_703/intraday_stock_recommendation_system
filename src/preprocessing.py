import pandas as pd


class StockDataPreprocessor:
    def __init__(self):
        pass

    @staticmethod
    def clean_data(df):
        # Perform data cleaning operations on the DataFrame
        # Remove duplicates, handle missing values, etc.
        # Example:
        df.drop_duplicates(inplace=True)
        df.dropna(inplace=True)

        return df

    def normalize_data(self, df):
        # Perform data normalization operations on the DataFrame
        # Normalize numerical features to a common scale
        # Example:
        numeric_cols = df.select_dtypes(include=['float', 'int']).columns
        df[numeric_cols] = (df[numeric_cols] - df[numeric_cols].min()) / (
                    df[numeric_cols].max() - df[numeric_cols].min())

        return df

    def preprocess(self, df):
        # Apply a series of preprocessing steps to the DataFrame
        # Example:
        df = self.clean_data(df)
        df = self.normalize_data(df)

        return df


def main():
    # Read the raw stock data from a CSV file
    file_path = "data/raw/stock1.csv"
    df = pd.read_csv(file_path)

    # Create an instance of the StockDataPreprocessor class
    preprocessor = StockDataPreprocessor()

    # Preprocess the data
    preprocessed_df = preprocessor.preprocess(df)

    # Save the preprocessed data as a CSV file
    preprocessed_file_path = "data/processed/stock1_processed.csv"
    preprocessed_df.to_csv(preprocessed_file_path, index=False)

    print("Data preprocessing completed.")


if __name__ == "__main__":
    main()
