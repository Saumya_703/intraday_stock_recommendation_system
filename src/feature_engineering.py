import pandas as pd


class FeatureEngineer:
    def __init__(self):
        pass

    def calculate_returns(self, df):
        # Calculate the daily returns based on the closing prices
        df['Returns'] = df['Close'].pct_change()

        return df

    def calculate_volatility(self, df, window=30):
        # Calculate the rolling volatility based on the daily returns
        df['Volatility'] = df['Returns'].rolling(window).std()

        return df

    def add_technical_indicators(self, df):
        # Add technical indicators to the DataFrame
        # Example:
        df['SMA'] = df['Close'].rolling(window=50).mean()
        df['RSI'] = self.calculate_rsi(df['Close'])

        return df

    def calculate_rsi(self, close_prices, window=14):
        # Calculate the Relative Strength Index (RSI) using the close prices
        # Example:
        price_diff = close_prices.diff(1)
        gain = price_diff.where(price_diff > 0, 0)
        loss = -price_diff.where(price_diff < 0, 0)
        avg_gain = gain.rolling(window).mean()
        avg_loss = loss.rolling(window).mean()
        rs = avg_gain / avg_loss
        rsi = 100 - (100 / (1 + rs))

        return rsi

    def engineer_features(self, df):
        # Apply feature engineering techniques to the DataFrame
        # Example:
        df = self.calculate_returns(df)
        df = self.calculate_volatility(df)
        df = self.add_technical_indicators(df)

        return df


def main():
    # Read the preprocessed stock data from a CSV file
    file_path = "data/processed/stock1_processed.csv"
    df = pd.read_csv(file_path)

    # Create an instance of the FeatureEngineering class
    fe = FeatureEngineer()

    # Engineer features
    engineered_df = fe.engineer_features(df)

    # Save the engineered features as a CSV file
    engineered_file_path = "data/processed/stock1_engineered.csv"
    engineered_df.to_csv(engineered_file_path, index=False)

    print("Feature engineering completed.")


if __name__ == "__main__":
    main()
