import pandas as pd
from sklearn.svm import SVC
from sklearn.model_selection import train_test_split
from sklearn.metrics import classification_report


class ModelTrainer:
    def __init__(self):
        pass

    def split_data(self, df, target_col):
        # Split the data into features (X) and target (y)
        X = df.drop(target_col, axis=1)
        y = df[target_col]

        # Split the data into training and testing sets
        X_train, X_test, y_train, y_test = train_test_split(X, y, test_size=0.2, random_state=42)

        return X_train, X_test, y_train, y_test

    def train_model(self, X_train, y_train):
        # Train a Support Vector Machine (SVM) model
        model = SVC()
        model.fit(X_train, y_train)

        return model

    def evaluate_model(self, model, X_test, y_test):
        # Evaluate the trained model on the testing data
        y_pred = model.predict(X_test)
        report = classification_report(y_test, y_pred)

        return report


def main():
    # Read the engineered stock features from a CSV file
    file_path = "data/processed/stock1_engineered.csv"
    df = pd.read_csv(file_path)

    # Specify the target column
    target_col = "Action"

    # Create an instance of the ModelTrainer class
    trainer = ModelTrainer()

    # Split the data into training and testing sets
    X_train, X_test, y_train, y_test = trainer.split_data(df, target_col)

    # Train the model
    model = trainer.train_model(X_train, y_train)

    # Evaluate the model
    evaluation_report = trainer.evaluate_model(model, X_test, y_test)

    print("Model training and evaluation completed.")
    print("Evaluation Report:")
    print(evaluation_report)


if __name__ == "__main__":
    main()
