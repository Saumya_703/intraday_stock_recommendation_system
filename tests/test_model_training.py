import unittest
import pandas as pd
from sklearn.svm import SVC
from src.model_training import ModelTrainer

class TestModelTraining(unittest.TestCase):
    def setUp(self):
        # Create a sample DataFrame for testing
        data = {
            'Date': ['2021-01-01', '2021-01-02', '2021-01-03'],
            'Open': [100.0, 110.0, 120.0],
            'High': [105.0, 115.0, 125.0],
            'Low': [95.0, 105.0, 115.0],
            'Close': [102.0, 112.0, 122.0],
            'Volume': [1000000, 1200000, 1500000],
            'Action': ['Buy', 'Sell', 'Hold']
        }
        self.df = pd.DataFrame(data)

    def test_train_model(self):
        mt = ModelTrainer()
        X_train, y_train = mt.prepare_train_data(self.df)

        model = SVC()
        model.fit(X_train, y_train)

        self.assertIsNotNone(model)

    def test_evaluate_model(self):
        mt = ModelTrainer()
        X_train, y_train = mt.prepare_train_data(self.df)
        X_test, y_test = mt.prepare_test_data(self.df)

        model = SVC()
        model.fit(X_train, y_train)

        accuracy = mt.evaluate_model(model, X_test, y_test)

        self.assertTrue(accuracy >= 0.0 and accuracy <= 1.0)

if __name__ == '__main__':
    unittest.main()
