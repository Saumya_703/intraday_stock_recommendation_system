import unittest
import pandas as pd
from src.feature_engineering import FeatureEngineer

class TestFeatureEngineering(unittest.TestCase):
    def setUp(self):
        # Create a sample DataFrame for testing
        data = {
            'Date': ['2021-01-01', '2021-01-02', '2021-01-03'],
            'Open': [100.0, 110.0, 120.0],
            'High': [105.0, 115.0, 125.0],
            'Low': [95.0, 105.0, 115.0],
            'Close': [102.0, 112.0, 122.0],
            'Volume': [1000000, 1200000, 1500000]
        }
        self.df = pd.DataFrame(data)

    def test_calculate_returns(self):
        fe = FeatureEngineer()
        df_with_returns = fe.calculate_returns(self.df)

        expected_returns = [0.02, 0.01818, 0.01786]
        actual_returns = df_with_returns['Returns'].tolist()

        self.assertEqual(actual_returns, expected_returns)

    def test_calculate_volatility(self):
        fe = FeatureEngineer()
        df_with_volatility = fe.calculate_volatility(self.df, window=2)

        expected_volatility = [0.03536, 0.02000, 0.01414]
        actual_volatility = df_with_volatility['Volatility'].tolist()

        self.assertEqual(actual_volatility, expected_volatility)

    def test_add_sma(self):
        fe = FeatureEngineer()
        df_with_sma = fe.add_sma(self.df, window=3)

        expected_sma = [103.33333, 111.0, 118.66667]
        actual_sma = df_with_sma['SMA'].tolist()

        self.assertEqual(actual_sma, expected_sma)

if __name__ == '__main__':
    unittest.main()
